package example7;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;

import de.felixroske.jfxsupport.FXMLController;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

@FXMLController
public class HelloworldController implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @FXML
    Pane myDynamicPane;
    
    @Autowired
    SomeButtonsView someButtonsView;
    
    @Autowired
    SomeOtherView someOtherView;

    public void showSomeButtonView(final Event e) {
        myDynamicPane.getChildren().clear();
        myDynamicPane.getChildren().add(applicationContext.getBean(SomeButtonsView.class).getView());
    }
    
    public void showSomeOtherView(final Event e) {
        myDynamicPane.getChildren().clear();
        myDynamicPane.getChildren().add(applicationContext.getBean(SomeOtherView.class).getView());
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
