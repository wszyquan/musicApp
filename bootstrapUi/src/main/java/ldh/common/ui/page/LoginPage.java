package ldh.common.ui.page;

import javafx.animation.*;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import ldh.fx.component.LWindowBase;
import ldh.fxanimations.FadeOutRightBigTransition;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

public class LoginPage extends LWindowBase {

    @FXML private Region topRectangle;
    @FXML private Region buttomRectangle;
    @FXML private GridPane loginPane;

    private Rectangle clip = new Rectangle();
    private Consumer<?> consumer = null;

    private Stage STAGE;

    public LoginPage(double width, double height) {
        super();
        buildMovable(this);
        this.setPrefHeight(height);this.setMinHeight(height);
        this.setPrefWidth(width);this.setMinWidth(width);

        loadFxl();
//        topRectangle.setWidth(width-3);
//        topRectangle.setHeight(height/2-1);
//        buttomRectangle.setWidth(width-3);
//        buttomRectangle.setHeight(height/2-2);

        AnchorPane.setBottomAnchor(topRectangle, height/2);
        AnchorPane.setTopAnchor(buttomRectangle, height/2);

        AnchorPane.setTopAnchor(loginPane, (height-loginPane.getPrefHeight())/2);
        AnchorPane.setLeftAnchor(loginPane, (width-loginPane.getPrefWidth())/2);

        clip.setWidth(width);
        clip.setHeight(height);
        this.setClip(clip);

    }

    public void setConsumer(Consumer<?> consumer) {
        this.consumer = consumer;
    }

    private void loadFxl() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/page/LoginPage.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setStage(Stage stage) {
        this.STAGE = stage;
    }

    @FXML
    private void loginAction() throws Exception {
        close((Void)->{
            if (consumer != null) {
                consumer.accept(null);
            }
        });
    }

    @FXML
    public void closeBtn() {
        Thread pngThread =  new Thread(()->{
            int i=0;
            while(i<20) {
                i++;
                String n = i + "";
                Platform.runLater(()->{
                    snapshot(LoginPage.this, n);
                });
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });
        Timeline timeline = new Timeline();
        KeyValue keyValue = new KeyValue(clip.xProperty(), this.getWidth());
        KeyFrame keyFrame = new KeyFrame(Duration.millis(150d), (e)->{
            STAGE.close();
        }, keyValue);
        timeline.getKeyFrames().add(keyFrame);
//        timeline.playFromStart();
//        pngThread.start();

//        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(200d), this);
//        translateTransition.setInterpolator(Interpolator.EASE_OUT);
//        translateTransition.setFromX(0);
////        translateTransition.setFromY(0);
//        translateTransition.setToX(this.getWidth());
////        translateTransition.setToY(this.getHeight());
//        translateTransition.setOnFinished(e->STAGE.close());
////        translateTransition.setDuration(Duration.millis(200d));
//        translateTransition.play();
//        STAGE.close();

        close(null);
    }

    private void close(Consumer<?> consumer) {
        FadeOutRightBigTransition fadeOutRightTransition = new FadeOutRightBigTransition(this);
        fadeOutRightTransition.setOnFinished(e->{
            STAGE.close();
            if (consumer != null) {
                consumer.accept(null);
            }
        });
        fadeOutRightTransition.playFromStart();
    }

    public void snapshot(Node view, String name) {
        Image image = view.snapshot(null, null);
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png",
                    new File("f:\\ttt\\" + name + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
