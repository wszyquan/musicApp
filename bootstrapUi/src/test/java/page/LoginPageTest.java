package page;

import com.jfoenix.controls.JFXDecorator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import ldh.common.ui.JavafxHomeApplication;
import ldh.common.ui.page.LoginPage;
import ldh.fxanimations.FadeInRightBigTransition;
import ldh.fxanimations.FadeOutUpBigTransition;


public class LoginPageTest extends Application {

    public static Stage STAGE = null;

    public void start(Stage primaryStage) throws Exception {
        Region node = FXMLLoader.load(JavafxHomeApplication.class.getResource("/fxml/Home.fxml"));
        JFXDecorator jfxDecorator = new JFXDecorator(primaryStage, node);
        jfxDecorator.setOnCloseButtonAction(()->{
            FadeOutUpBigTransition fadeOutUpBigTransition = new FadeOutUpBigTransition(jfxDecorator);
            fadeOutUpBigTransition.setOnFinished(e->primaryStage.close());
            fadeOutUpBigTransition.playFromStart();
        });
        Scene rscene = new Scene(jfxDecorator, 1200, 700);
        rscene.setFill(null);
        primaryStage.setScene(rscene);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        jfxDecorator.setOpacity(0);
        STAGE = primaryStage;

        showLogin(primaryStage, jfxDecorator);
    }

    private void showLogin(Stage primaryStage, Node node) {
        Stage stage = new Stage();
        stage.initOwner(primaryStage);
        LoginPage loginPage = new LoginPage(450, 420);
        loginPage.setConsumer((Void)->{
            primaryStage.show();
            FadeInRightBigTransition fadeInRightTransition = new FadeInRightBigTransition(node);
            fadeInRightTransition.playFromStart();
        });
        loginPage.setStage(stage);
        Scene scene = new Scene(loginPage);
        stage.setScene(scene);
        scene.setFill(null);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.centerOnScreen();

        loginPage.setOpacity(0);
        stage.show();

        FadeInRightBigTransition fadeInRightTransition = new FadeInRightBigTransition(loginPage);
        fadeInRightTransition.setDelay(new Duration(800));
        fadeInRightTransition.playFromStart();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
