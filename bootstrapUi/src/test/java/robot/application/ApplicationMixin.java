package robot.application;

import javafx.scene.Scene;
import robot.util.GlassRobot;
import robot.util.GlassRobotImpl;
import robot.util.SceneRobot;
import robot.util.SceneRobotImpl;

public class ApplicationMixin {

    public static GlassRobot createGlassRobot() {
        return new GlassRobotImpl();
    }

    public static SceneRobot createSceneRobot(Scene scene) {
        return new SceneRobotImpl(scene);
    }

}
