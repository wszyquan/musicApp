package animation;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.PerspectiveTransform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class PerspectiveTransformTest extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        ImageView image1 = new ImageView(new Image(getClass().getResourceAsStream("/pic/0.jpg")));
        ImageView image2 = new ImageView(new Image(getClass().getResourceAsStream("/pic/1.jpg")));
        FlipView flip = new FlipView(image1, image2);
        VBox vBox = new VBox();
        Button button = new Button("run");
        button.setOnAction(e->flip.forward());
        vBox.getChildren().addAll(button, flip);
        Scene scene = new Scene(vBox, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private class FlipView extends Group {

        Timeline timeline = new Timeline();

        //正面视图
        public Node frontNode;
        //反面视图
        public Node backNode;
        //是否翻转
        boolean flipped = false;
        //翻转角度
        DoubleProperty time = new SimpleDoubleProperty(Math.PI / 2);
        //正面翻转特效
        PerspectiveTransform frontEffect = new PerspectiveTransform();
        //反面翻转特效
        PerspectiveTransform backEffect = new PerspectiveTransform();

        public FlipView(ImageView imageView1, ImageView imageView2) {
            this.frontNode = imageView1;
            this.backNode = imageView2;
            init();
        }

        public void forward() {
            timeline.playFromStart();
        }

        private void init() {
            KeyFrame frame1 = new KeyFrame(Duration.ZERO, new KeyValue(time,
                    Math.PI / 2, Interpolator.LINEAR));
            KeyFrame frame2 = new KeyFrame(Duration.seconds(1), (e)->{flipped = !flipped;}, new KeyValue(time, -Math.PI / 2, Interpolator.LINEAR));

            timeline.getKeyFrames().addAll(frame1, frame2);

            create();
        }

        private void create() {
            time.addListener((b, o, n)->{
                setPT(frontEffect, time.get());
                setPT(backEffect, time.get());
            });

            backNode.visibleProperty().bind(
                    Bindings.when(time.lessThan(0)).then(true).otherwise(false));

            frontNode.visibleProperty().bind(
                    Bindings.when(time.lessThan(0)).then(false).otherwise(true));
            setPT(frontEffect, time.get());
            setPT(backEffect, time.get());
            frontNode.setEffect(frontEffect);
            backNode.setEffect(backEffect);
            getChildren().addAll(backNode, frontNode);
        }

        private void setPT(PerspectiveTransform pt, double t) {
            double width = 200;
            double height = 200;
            double radius = width / 2;
            double back = height / 10;
            pt.setUlx(radius - Math.sin(t) * radius);
            pt.setUly(0 - Math.cos(t) * back);
            pt.setUrx(radius + Math.sin(t) * radius);
            pt.setUry(0 + Math.cos(t) * back);
            pt.setLrx(radius + Math.sin(t) * radius);
            pt.setLry(height - Math.cos(t) * back);
            pt.setLlx(radius - Math.sin(t) * radius);
            pt.setLly(height + Math.cos(t) * back);
        }
    }
}
