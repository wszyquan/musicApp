package animation;

import javafx.animation.*;
import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Point3D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.apache.commons.lang3.RandomUtils;

import java.util.Random;

public class TimelineTest extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Timeline timeline = new Timeline();
        SequentialTransition sequentialTransition=new SequentialTransition(timeline);
        timeline.setCycleCount(Animation.INDEFINITE);

        VBox vBox = new VBox();
        Slider slider = new Slider(0, 25, 0.1);
        slider.valueChangingProperty().addListener((b, o, n)->{

            System.out.println("tt:" + slider.getValue());
            sequentialTransition.jumpTo(Duration.seconds(slider.getValue()));
            sequentialTransition.play();
//            timeline.jumpTo(Duration.seconds(slider.getValue()));
            sequentialTransition.stop();
        });
        StackPane group  = new StackPane();
        group.setAlignment(Pos.TOP_LEFT);
        vBox.getChildren().addAll(slider, group);
        initGroup(group, timeline);
        VBox.setVgrow(group, Priority.ALWAYS);
        Scene scene = new Scene(vBox, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.show();

//        timeline.playFromStart();

        sequentialTransition.playFromStart();
//        timeline.jumpTo(Duration.seconds(1.6));
//        timeline.stop();
    }

    private void initGroup(Pane group, Timeline timeline) {
        group.setClip(new Rectangle(600, 400));
        for (int i=0; i<6; i++) {
            String p = TimelineTest.class.getResource("/pic/" + i + ".jpg").toExternalForm();
            Image image = new Image(p);
            ImageView imageView = new ImageView(image);
            imageView.setFitHeight(400);
            imageView.setFitWidth(600);
            imageView.setClip(new Rectangle(600, 400));
            group.getChildren().add(imageView);

            int n = RandomUtils.nextInt(0, 100);
            if (n%2==0) {
                imageView.setTranslateX(-600);

                KeyFrame keyFrame = new KeyFrame(Duration.seconds(i * 5), new KeyValue(imageView.translateXProperty(), -600));
                KeyFrame keyFrame4 = new KeyFrame(Duration.seconds(i * 5), new KeyValue(imageView.scaleXProperty(), 0.3));
                KeyFrame keyFrame41 = new KeyFrame(Duration.seconds(i * 5), new KeyValue(imageView.scaleYProperty(), 0.3));
                KeyFrame keyFrame5 = new KeyFrame(Duration.seconds(i * 5), new KeyValue(imageView.rotateProperty(), 0));
                KeyFrame keyFrame51 = new KeyFrame(Duration.seconds(i * 5+0.5), new KeyValue(imageView.rotateProperty(), 360));
                KeyFrame keyFrame52 = new KeyFrame(Duration.seconds(i * 5+1), new KeyValue(imageView.rotateProperty(), 720));
                KeyFrame keyFrame53 = new KeyFrame(Duration.seconds(i * 5+1.5), new KeyValue(imageView.rotateProperty(), 1080));
                KeyFrame keyFrame54 = new KeyFrame(Duration.seconds(i * 5+2), new KeyValue(imageView.rotateProperty(), 1440));

                KeyFrame keyFrame2 = new KeyFrame(Duration.seconds(i * 5 + 2), new KeyValue(imageView.translateXProperty(), 0));
                KeyFrame keyFrame3 = new KeyFrame(Duration.seconds(i * 5 + 2), new KeyValue(imageView.scaleXProperty(), 1, Interpolator.EASE_IN));
                KeyFrame keyFrame31 = new KeyFrame(Duration.seconds(i * 5 + 2), new KeyValue(imageView.scaleYProperty(), 1, Interpolator.EASE_IN));
                timeline.getKeyFrames().addAll(keyFrame, keyFrame2, keyFrame4, keyFrame41, keyFrame5, keyFrame51, keyFrame52, keyFrame53, keyFrame54, keyFrame3, keyFrame31);
            } else {
//                imageView.setTranslateX(50);
//                imageView.setTranslateY(50);
                imageView.setOpacity(0);
//                imageView.setRotationAxis(new Point3D(200, 300, 0));

                IntegerProperty iteration = new SimpleIntegerProperty(3);
                BoxBlur bb = new BoxBlur();
                bb.setWidth(5);
                bb.setHeight(5);
                bb.iterationsProperty().bindBidirectional(iteration);
                imageView.setEffect(bb);

                KeyFrame keyFrame = new KeyFrame(Duration.seconds(i * 5), new KeyValue(iteration, 6));
                KeyFrame keyFrame1 = new KeyFrame(Duration.seconds(i * 5+1), new KeyValue(iteration, 3));
                KeyFrame keyFrame12 = new KeyFrame(Duration.seconds(i * 5+2), new KeyValue(iteration, 0));

                KeyFrame keyFrame6 = new KeyFrame(Duration.seconds(i * 5), new KeyValue(imageView.opacityProperty(), 0));
                KeyFrame keyFrame61 = new KeyFrame(Duration.seconds(i * 5+2), new KeyValue(imageView.opacityProperty(), 1));

                KeyFrame keyFrame4 = new KeyFrame(Duration.seconds(i * 5), new KeyValue(imageView.scaleXProperty(), 0.1));
                KeyFrame keyFrame41 = new KeyFrame(Duration.seconds(i * 5), new KeyValue(imageView.scaleYProperty(), 0.1));

                KeyFrame keyFrame5 = new KeyFrame(Duration.seconds(i * 5), new KeyValue(imageView.rotateProperty(), 0));
                KeyFrame keyFrame51 = new KeyFrame(Duration.seconds(i * 5+0.5), new KeyValue(imageView.rotateProperty(), 360));
                KeyFrame keyFrame52 = new KeyFrame(Duration.seconds(i * 5+1), new KeyValue(imageView.rotateProperty(), 720));
                KeyFrame keyFrame53 = new KeyFrame(Duration.seconds(i * 5+1.5), new KeyValue(imageView.rotateProperty(), 1080));
//                KeyFrame keyFrame54 = new KeyFrame(Duration.seconds(i * 5+2), new KeyValue(imageView.rotateProperty(), 1440));

                KeyFrame keyFrame2 = new KeyFrame(Duration.seconds(i * 5 + 2), new KeyValue(imageView.translateXProperty(), 0));
                KeyFrame keyFrame21 = new KeyFrame(Duration.seconds(i * 5 + 2), new KeyValue(imageView.translateYProperty(), 0));

                KeyFrame keyFrame3 = new KeyFrame(Duration.seconds(i * 5 + 2), new KeyValue(imageView.scaleXProperty(), 1, Interpolator.EASE_IN));
                KeyFrame keyFrame31 = new KeyFrame(Duration.seconds(i * 5 + 2), new KeyValue(imageView.scaleYProperty(), 1, Interpolator.EASE_IN));
                timeline.getKeyFrames().addAll(keyFrame, keyFrame1, keyFrame12, keyFrame4, keyFrame41, keyFrame3, keyFrame31, keyFrame5, keyFrame51, keyFrame52, keyFrame53
                        , keyFrame6,keyFrame61); //, keyFrame54
            }

        }

    }


}
